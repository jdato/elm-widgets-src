# elm-widgets-sample
A sample project to : https://github.com/remoteradio/elm-widgets.git
demo: https://obscure-sands-3870.herokuapp.com/

Setting up:
```sh
<project root>/ bundle install
<project root>/examples/elm/elm-package install
```

Running app:
```sh
<project root>/ rackup
```

To edit elm-widgets execute the following and save /examples/elm/main.elm to auto compile the elm file
```sh
<project root>/ ./compile-auto-elm-example.sh
```

Project Requirements:
ruby '2.1.3'
